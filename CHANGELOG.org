* 3.0.2
** Features
*** ADD-PROPERTY removed. Replaced by (SETF PROPERTY-VALUE)
    This means property-setting semantics have changed a bit. Refer to the documentation.
*** Optimization
    Lots of stuff has been sped up. Particularly, object creation and reply dispatch.
*** Documentation
    The manual has been updated.
** Bugfixes
* 3.0.1
** Features
*** Improved INIT trio of messages with a better API
*** Exported CLONE as part of the API
*** Improved examples
*** Lots of internal cleanups
** Bugfixes
   Minor bugfixes here and there, apart from init-trio stuff.
* 3.0
** Features
*** Complete reimplementation
    Sheeple itself has been reimplemented (again). It runs faster and lighter than ever.
*** Various interface changes
    Various functions have been renamed, others changed at an interface level.
** Bugfixes
* 2.2
** Features
*** PROPERTY-SPEC works more like the CLOS MOP's slotd-based stuff now.
*** PROPERTY-SPEC -> STANDARD-PROPERTY
*** An initial (sort of-)working implementation of a property MOP done
*** Beginnings of unit tests
*** DEFPROTO works like DEFSHEEP now -- all protos are kept in the value namespace
** Bugs
*** Mostly bugs involving new property metaobject code...
* 2.1.2
** Features
*** More unit tests
** Bugfixes
*** INIT-SHEEP and REINIT-SHEEP work more as expected now.
*** Orphaned sheep are printable now.
* 2.1.1
** Features
** Bugs
*** Fixed a bug where &aok was appended incorrectly to replies without &key
* 2.1
** Features
*** No more #@ for accessing prototypes
*** PROTO replaced FIND-PROTO
** Bugs
*** dispatch failures weren't being very helpful.
* 2.0.1
** Features
   NIL
** Bugs
*** Fixed has-property-p
*** More useful error when a does not receive enough args
* 2.0
** Features
   Completely new interface, more MOP stuff. Lots of new tests.
** Bugs
   Probably created more bugs than I fixed.
* 1.1.1
** Features
*** Minor tweaks. This version is mostly a checkpoint pre-2.0
** Bugs
   NIL
* 1.1
** Features
*** Sheeple rewritten using CLOS instead of structs/funs
*** The beginnings of a MOP has been exposed in the API.
*** Cloneforms deprecated.
*** Documentation updated
** Bugs
*** Maybe fixed: there was a problem with how things were getting evaluated.
*** nil lambda-list buzzwords weren't dispatching.
*** small bugfixes and optimizations here and there
* 1.0.1
** Features
*** Added docstring support to messages. No way to grab them yet, but at least it doesn't complain.
*** CLONE/CLONE*/DEFSHEEP now support a gimpy :documentation option.
** Bugs
*** Dispatch caching was keeping references around. Switched to weak pointers.
*** Renamed who-sets to property-owner (conflict with CL function)
*** :around messages were horribly broken. This has been fixed.
*** Sheeple mass-produced wolves. This has been fixed.
*** All the undefined-variable warnings are gone now.
* 1.0
** Features
*** New and improved guid available in doc/user-guide.org!
*** New CLONE* macro
**** :accessor can be used now, and is preferred. :manipulator is deprecated.
**** with-accessors works (it did before, too), not just with-manipulators.
**** CLONE* works more like DEFCLASS*. It auto-generates accessors unless told not to do so.
**** DEFSHEEP now uses CLONE* instead of CLONE.
*** Updated API -- no longer exporting some symbols, and exporting other new ones
*** Sheep locking
    Sheep can be locked by doing lock-sheep/unlock-sheep/toggle-sheep-lock
*** Undefinition works different
    Message/buzzword undefinition no longer works as before. Undefbuzzword does not remove roles, it
    simply removes the buzzword from the global table and makes the function unbound.
*** Improved AVAILABLE-MESSAGES
    the function now helpfully returns a list of vectors. The first entry is the buzzword name, the
    second is the position the sheep can participate in.
*** UNDEFBUZZWORD removed
** Bugs
*** Made next-messages ignorable. Compilers should no longer warn about it.
*** There was a bug in CCL with ensure-buzzword complaining about :documentation
*** Added another check to CLONE. Can no longer have :reader/:writer/:accessor as nil, as well as define one.
*** MITOSIS works a bit more like expected, but still doesn't recursively copy collections.
*** UNDEFMESSAGE works correctly, after some minor hiccups.

* 0.9
** Features
*** HUGE optimization for message dispatch.
*** Property-access is no longer cached -- setfing was way too expensive.
    It will stay thing way unless I figure out a good lazy-update scheme.
*** Property-access itself is a little faster now, too
** Bugs
*** Shit wasn't updating properly before, so bugs could crop up with deep hierarchies.
*** Little stability tweaks here and there. Doesn't break as often :)
*** Buzzwords with no arglist work now (snort)
*** Buzzword redefinition now checks for lambda-list consistency with existing messages.

* 0.8
** Features
*** Significant speedup for message dispatch, using a simple caching system.
** Bugs
*** &allow-other-keys is no longer necessary when using &key args with buzzwords.

* 0.7
** Features
*** Can now define messages on initialize-sheep and reinitialize-sheep
*** Message lambda-list congruence now follows the same rules as CLOS:
    http://www.lispworks.com/documentation/HyperSpec/Body/07_fd.htm
** Bugs
*** buzzwords with &key, &rest, etc, actually work now
*** some mostly-inconsequential whoopsies with conditions were patched

* 0.6
** Features
*** Nothing worth noting
** Bugfixes
*** defsheep wasn't actually redefining sheeple.
*** Lots of minor bugfixes

* 0.5
** Features
*** Cloneforms actually work more like CLOS initforms now
    (they don't execute if a value is already provided)
*** Major, faster reimplementation of Sheeple. The MOP stuff is gone for now, though.
** Bugfixes
*** Conditions work nicer now. Reorganized them and put them in their place.

* 0.4
** Features
*** Several MOP-related symbols made available, including sheep creation and property access.
*** More information about Sheeple in README
** Bugfixes
*** Fixed a bug with undefbuzzword that undefined all messages

* 0.3
** Features
*** CLOS-style lambda-lists for buzzwords and messages implemented
*** Updated README to reflect new defbuzzword
*** More tests written -- all pass on SBCL/Lin32
** Bugfixes
*** Fixed issue with message blocks
*** Fixed issue caused by fixing issue with message blocks

* 0.2
** Features
*** with-properties and with-manipulators implemented
*** small code cleanup
*** New tools for inspecting and manipulating cloneforms
*** Property access speedup -- all property keys must now be symbols
** Bugfixes
*** NIL

* 0.1
  Initial release
